from django.contrib import admin
from django.urls import path

from . import views

app_name = "camera"

urlpatterns = [
    path('', views.index, name = "camera_input"),
    path('input_form', views.input_form, name = "input_form")
]