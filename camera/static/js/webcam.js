'use strict';

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.getElementById('span#errorMsg');

// check whether we can use facingMode
let faceMode = document.getElementById("input_cam")
let supports = navigator.mediaDevices.getSupportedConstraints();
if( supports['facingMode'] === true ) {
  faceMode.disabled = false;
}
// console.log(faceMode.value)

let constraints = {
    video: {
        width: 1280, height: 720, facingMode: faceMode.value
    }
};


function capture(){
    // Access webcam
    async function init() {
        try {
            const stream = await navigator.mediaDevices.getUserMedia(constraints);
            handleSuccess(stream);
        } catch (e) {
            errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
        }
    }

    // Success
    function handleSuccess(stream) {
        console.log(constraints)
        window.stream = stream;
        video.srcObject = stream;
        video.play();
    }

    // Load init
    init();

    // Draw image
    var context = canvas.getContext('2d');
    snap.addEventListener("click", function() {
        context.drawImage(video, 0, 0, 640, 480);
    });
}

faceMode.addEventListener("change", function() {
    video.srcObject.getTracks().forEach(t => {
        t.stop();
      });
    video.srcObject = null
    constraints = {
        video: {
            width: 1280, height: 720, facingMode: faceMode.value
        }
    };
    // console.log(constraints)
    
    capture()
    
});

capture()